$(document).ready(function() {
    
    
    //Main Menu
    $(".menu-btn").click(function(){
        $(this).toggleClass("is-active");
        $(".main-nav").toggleClass("visible");
        $("body").toggleClass("noScroll");
        $(".main-content").toggleClass("open");
    });
    
    /* Notification */
    $(".notification .dismiss").click(function(){
        $(this).closest(".notification").slideUp(100);
    });
    
    //Form
    $(".form-item input, .form-item textarea").focusin(function(){
        $(this).parent(".form-item").addClass("active");
    });
    
    $(".form-item input, .form-item textarea").focusout(function(){
        $(this).parent(".form-item").removeClass("active");
    });
    
    //Form Validation
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('form');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    
    /* FIle Upload */
    $(".file-upload").on("change", "input", function(){ 
        $(this).closest(".file-upload").find(".name").text( $(this).val().replace(/.*(\/|\\)/, '') );
        $(this).closest(".file-upload").find(".select-file").toggle();
        $(this).closest(".file-upload").find(".file-preview").toggle();
    });
    $(".file-upload .trash").on('click', function() { 
        console.log("Trashed");
        $(this).closest(".file-upload").find(".name").text( 'Select your file!' );
        $(this).closest(".file-upload").find("input").val('');

        $(this).closest(".file-upload").find(".select-file").toggle();
        $(this).closest(".file-upload").find(".file-preview").toggle();
    });
    
    //Popover
    $('[data-toggle="popover"]').popover({
        container: '.content-area'
    });
     
     
    // Image Upload Preview
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);

          $('#blah').hide();
          $('#blah').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imgInp").change(function() {
      readURL(this);
    });
     
     
    
    //Remove Pre-loader
    $('.pre-loader').delay(500).fadeOut();
    
    //Wow JS
    new WOW().init();
    
    //Swiper
    var swiper = new Swiper('.home-slider', {
        spaceBetween: 0,
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
        speed: 400,
        loop: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        /*pagination: {
            el: '.swiper-pagination-home',
            clickable: true,
        },*/
    });
    

    

    
    
});